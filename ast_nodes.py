from constants import *
from output import Output
from context import Context
import copy

class AST_Node:
    records = copy.deepcopy(USABLERECORDS)
    tagIndex = 0
    context = Context()
        
    def __init__(self):
        pass

    @classmethod
    def getRecord(cls):
        try:
            record = cls.records.pop(0)
        except Exception as e:
            raise Exception('Not enough records to compile the program')
        cls.context.useRecord(record)
        return record
        
    @classmethod
    def recoverRecord(cls, record):
        if record in USABLERECORDS:
            cls.records.insert(0,record)
            cls.context.recoverRecord(record)

    @classmethod
    def getIdIndex(cls, anId):
        return cls.context.getIdIndex(anId)

    def __repr__(self):
        return str(self)

class Program(AST_Node):
    def __init__(self, functions):
        AST_Node.__init__(self)
        self.functions = functions
        self.externFuns = [EXIT] + list(set(self.getExternFuns()))

    def getExternFuns(self):
        return reduce(lambda acc, fun: acc + fun.getExternFuns(), self.functions, [])

    def __str__(self):
        text = ''
        if self.functions:
            if PRINTF in self.externFuns:
                text += 'section .data\n' + LLI + '  db  " % lli "\n'
            text += 'section .text\n' + 'global main\n'
            text += 'extern ' + ', '.join(self.externFuns) + '\n'
            text += reduce(lambda acc, x: acc + str(x), self.functions, '')
            text += Output.fun_def('main', '')
            text += Output.cuca_call('main')
            text += Output.mov(RDI,0)
            text += Output.call(EXIT)
        return text

class Function(AST_Node):
    def __init__(self, name, params, rType, block):
        AST_Node.__init__(self)
        self.name = name
        self.type = rType
        self.block = block
        self.params = params

    def getExternFuns(self):
        return self.block.getExternFuns()

    def allocateLocalVars(self):
        paramsNames = map(lambda param: param.name, self.params)
        localVars = list(set(self.block.getLocalVariables(paramsNames)))
        return Output.sub(RSP, len(localVars) * BYTES) if localVars else ''

    def __str__(self):
        AST_Node.context.reset()
        for param in self.params:
            AST_Node.context.addParam(param.name)
        
        text = Output.fun_def(self.name)
        text += Output.push(RBP)
        text += Output.mov(RBP,RSP)
        text += self.allocateLocalVars()
        text += str(self.block)
        text += Output.mov(RSP,RBP)
        text += Output.pop(RBP)
        text += Output.text('ret')

        return text

class Block(AST_Node):
    def __init__(self, statements):
        AST_Node.__init__(self)
        self.statements = statements

    def getExternFuns(self):
        funNames = []
        for stmt in self.statements:
            funNames.extend(stmt.getExternFuns())
        
        return funNames
        
    def getLocalVariables(self, params):
        varNames = []
        for stmt in self.statements:
            varNames.extend(stmt.getLocalVariables(params))

        return varNames

    def __str__(self):
        return reduce(lambda acc, x: acc + str(x), self.statements, '')


## Statements

class StmtAssign(AST_Node):
    def __init__(self, varId, exp):
        AST_Node.__init__(self)
        self.varId = varId
        self.exp = exp

    def getExternFuns(self):
        return []

    def getLocalVariables(self, params):
        return [self.varId] if not self.varId in params else []

    def __str__(self):
        index = AST_Node.context.addVar(self.varId)
        (expText, lastValueUsed) = self.exp.getText()
        text = expText
        text += Output.movAssign(index, lastValueUsed)
        AST_Node.recoverRecord(lastValueUsed)

        return text

class StmtVecAssign(AST_Node):
    def __init__(self, varId, indexExp, exp):
        AST_Node.__init__(self)
        self.varId = varId
        self.indexExp = indexExp
        self.exp = exp

    def getExternFuns(self):
        return []

    def getLocalVariables(self, params):
        return []

    def __str__(self):
        (indexExpText, indexExpLastValueUsed) = self.indexExp.getText()
        (expText, expLastValueUsed) = self.exp.getText()
        text = indexExpText + expText
        text += Output.mov(RDI, indexExpLastValueUsed)
        text += Output.mov(RAX, RDI)
        text += Output.inc(RAX)
        text += Output.sal(RAX, 3)
        text += Output.add(RAX,'[' + RBP + ' - ' + str(BYTES) + ']')
        text += Output.mov('[' + RAX + ']', expLastValueUsed)

        AST_Node.recoverRecord(indexExpLastValueUsed)
        AST_Node.recoverRecord(expLastValueUsed)

        return text

class StmtIf(AST_Node):
    def __init__(self, boolExp, block):
        AST_Node.__init__(self)
        self.boolExp = boolExp
        self.block = block

    def getExternFuns(self):
        return self.block.getExternFuns()

    def getLocalVariables(self, params):
        return self.block.getLocalVariables(params)

    def __str__(self):
        (expText, expLastValueUsed) = self.boolExp.getText()
        labelEnd = LABEL_END + str(AST_Node.tagIndex)
        AST_Node.tagIndex += 1

        text = expText
        text += Output.cmp(expLastValueUsed, 0)
        text += Output.condJump(JE, labelEnd)
        text += str(self.block)
        text += Output.tag_def(labelEnd)

        AST_Node.recoverRecord(expLastValueUsed)

        return text

class StmtIfElse(AST_Node):
    def __init__(self, boolExp, thenBlock, elseBlock):
        AST_Node.__init__(self)
        self.boolExp = boolExp
        self.thenBlock = thenBlock
        self.elseBlock = elseBlock

    def getExternFuns(self):
        return self.thenBlock.getExternFuns() + self.elseBlock.getExternFuns()

    def getLocalVariables(self, params):
        return self.thenBlock.getLocalVariables(params) + self.elseBlock.getLocalVariables(params)

    def __str__(self):
        (expText, expLastValueUsed) = self.boolExp.getText()
        labelElse = LABEL_ELSE + str(AST_Node.tagIndex)
        labelEnd = LABEL_END + str(AST_Node.tagIndex)
        AST_Node.tagIndex += 1

        text = expText
        text += Output.cmp(expLastValueUsed, 0)
        text += Output.condJump(JE, labelElse)
        text += str(self.thenBlock)
        text += Output.jmp(labelEnd)
        text += Output.tag_def(labelElse)
        text += str(self.elseBlock)
        text += Output.tag_def(labelEnd)

        AST_Node.recoverRecord(expLastValueUsed)

        return text

class StmtWhile(AST_Node):
    def __init__(self, boolExp, block):
        AST_Node.__init__(self)
        self.boolExp = boolExp
        self.block = block

    def getExternFuns(self):
        return self.block.getExternFuns()

    def getLocalVariables(self, params):
        return self.block.getLocalVariables(params)

    def __str__(self):
        (expText, expLastValueUsed) = self.boolExp.getText()
        labelStart = LABEL_START + str(AST_Node.tagIndex)
        labelEnd = LABEL_END + str(AST_Node.tagIndex)
        AST_Node.tagIndex += 1

        text = Output.tag_def(labelStart)
        text += expText
        text += Output.cmp(expLastValueUsed, 0)
        text += Output.condJump(JE, labelEnd)
        text += str(self.block)
        text += Output.jmp(labelStart)
        text += Output.tag_def(labelEnd)

        AST_Node.recoverRecord(expLastValueUsed)

        return text

class StmtReturn(AST_Node):
    def __init__(self, exp):
        AST_Node.__init__(self)
        self.exp = exp

    def getExternFuns(self):
        return []

    def getLocalVariables(self, params):
        return []

    def __str__(self):
        (expText, lastValueUsed) = self.exp.getText()
        text = expText
        text += Output.mov(RAX, lastValueUsed)
        AST_Node.recoverRecord(lastValueUsed)
        return text

class Call(AST_Node):
    def __init__(self, funId, params):
        AST_Node.__init__(self)
        self.funId = funId
        self.params = params

    def printCall(self, paramsTextTuples):
        text = ''
        if self.funId == CUCA_PUTCHAR:
            text += Output.mov(RDI, paramsTextTuples[0][1])
            text += Output.call(PUTCHAR)
        elif self.funId == CUCA_PUTNUM:
            text += Output.mov(RSI, paramsTextTuples[0][1])
            text += Output.mov(RDI, LLI)
            text += Output.mov(RAX, 0)
            text += Output.call(PRINTF)
        else:
            text += Output.cuca_call(self.funId)

        return text

class StmtCall(Call):
    def __init__(self, funId, params):
        Call.__init__(self, funId, params)

    def getExternFuns(self):
        return [EXTERNFUNS[self.funId]] if self.funId in EXTERNFUNS.keys() else []

    def getLocalVariables(self, params):
        return []

    def __str__(self):
        # Add each param text
        paramsTextTuples = map(lambda param: param.getText(), self.params)
        text = reduce(lambda acc, x: acc + x[0], paramsTextTuples, '')

        isExtern = self.funId in EXTERNFUNS.keys()

        if not isExtern:
            # Allocate params in pile
            for param in paramsTextTuples:
                text += Output.sub(RSP, BYTES)
                text += Output.mov('[' + RSP + ']', param[1])
                AST_Node.recoverRecord(param[1])

            # Push used Records
            for record in AST_Node.context.getUsedRecords():
                text += Output.push(record)

        # Call
        text += self.printCall(paramsTextTuples)

        if not isExtern:
            # Pop used Records
            for record in AST_Node.context.getUsedRecords():
                text += Output.pop(record)

        # Recover all params last Records used
        if isExtern:
            for param in paramsTextTuples:
                AST_Node.recoverRecord(param[1])
    
        return text

## Expresiones

class ExprInt(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def getType(self,program,context):
        return Int()

class ExprBool(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def getType(self,program,context):
        return Bool()

class ExprVec(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def getType(self,program,context):
        return Vec()

class ExprVar(AST_Node):
    def __init__(self, varId):
        AST_Node.__init__(self)
        self.varId = varId

    def getText(self):
        record = AST_Node.getRecord()
        (index, iType) = AST_Node.getIdIndex(self.varId)
        if (iType == PARAM):
            text = Output.movParam(record, index)
        else:
            text = Output.movVar(record, index)

        return (text, record)

    def getType(self,program,context):
        return context[self.varId]

class ExprConstNum(ExprInt):
    def __init__(self, constNum):
        ExprInt.__init__(self)
        self.constNum = constNum

    def getText(self):
        record = AST_Node.getRecord()
        text = Output.mov(record, self.constNum)
        return (text, record)

class ExprConstBool(ExprBool):
    def __init__(self, constBool):
        ExprBool.__init__(self)
        self.constBool = constBool

    def getText(self):
        record = AST_Node.getRecord()
        text = Output.mov(record, -1) if self.constBool else Output.mov(record, 0)
        return (text, record)

class ExprVecMake(ExprVec):
    def __init__(self, numExpressions):
        ExprVec.__init__(self)
        self.numExpressions = numExpressions

    def getText(self):
        vecSize = len(self.numExpressions)
        record = AST_Node.getRecord()

        text = Output.sub(RSP, (vecSize + 1) * BYTES)
        text += Output.mov(record, RSP)
        text += Output.vecStore(record, vecSize)
        expPos = 1
        for numExp in self.numExpressions:
            (expText, lastValueUsed) = numExp.getText()
            text += expText
            text += Output.vecStore(record + ' + ' + str(expPos * BYTES), lastValueUsed)
            expPos += 1
            AST_Node.recoverRecord(lastValueUsed)

        return (text, record)

class ExprVecLength(ExprInt):
    def __init__(self, vecId):
        self.vecId = vecId

    def getText(self):
        record = AST_Node.getRecord()
        (index, iType) = AST_Node.getIdIndex(self.vecId)
        if (iType == PARAM):
            text = Output.movParam(RAX, index)
        else:
            text = Output.movVar(RAX, index)
        text += Output.mov(record, '[' + RAX + ']')

        return (text, record)

class ExprVecDeref(ExprInt):
    def __init__(self, vecId, numExp):
        ExprInt.__init__(self)
        self.vecId = vecId
        self.numExp = numExp

    def getText(self):
        (expText, lastValueUsed) = self.numExp.getText()
        text = expText
        text += Output.mov(RDI, lastValueUsed)
        text += Output.mov(RAX, RDI)
        text += Output.inc(RAX)
        text += Output.sal(RAX, 3)
        text += Output.add(RAX,'[' + RBP + ' - ' + str(BYTES) + ']')
        text += Output.mov(RDI, '[' + RAX + ']')
        AST_Node.recoverRecord(lastValueUsed)

        return (text, RDI)

class ExprCall(Call):
    def __init__(self, funId, params):
        Call.__init__(self, funId, params)

    def getText(self):
        # Add each param text
        paramsTextTuples = map(lambda param: param.getText(), self.params)
        text = reduce(lambda acc, x: acc + x[0], paramsTextTuples, '')

        isExtern = self.funId in EXTERNFUNS.keys()

        if not isExtern:
            # Allocate params in pile
            for param in paramsTextTuples:
                text += Output.sub(RSP, BYTES)
                text += Output.mov('[' + RSP + ']', param[1])
                AST_Node.recoverRecord(param[1])

            # Push used Records
            for record in AST_Node.context.getUsedRecords():
                text += Output.push(record)

        # Call
        text += self.printCall(paramsTextTuples)

        if not isExtern:
            # Pop used Records
            for record in AST_Node.context.getUsedRecords():
                text += Output.pop(record)

        # Recover all params last Records used
        if isExtern:
            for param in paramsTextTuples:
                AST_Node.recoverRecord(param[1])
    
        return (text, RAX)

    def getType(self, program, context):
        return program.functions[self.funId].type


class ExprAnd(ExprBool):
    def __init__(self, boolExp1, boolExp2):
        ExprBool.__init__(self)
        self.boolExp1 = boolExp1
        self.boolExp2 = boolExp2

    def getText(self):
        (exp1Text, exp1LastValueUsed) = self.boolExp1.getText()
        (exp2Text, exp2LastValueUsed) = self.boolExp2.getText()
        text = exp1Text + exp2Text
        text += Output._and(exp1LastValueUsed, exp2LastValueUsed)
        AST_Node.recoverRecord(exp2LastValueUsed)
        return (text, exp1LastValueUsed)

class ExprOr(ExprBool):
    def __init__(self, boolExp1, boolExp2):
        ExprBool.__init__(self)
        self.boolExp1 = boolExp1
        self.boolExp2 = boolExp2

    def getText(self):
        (exp1Text, exp1LastValueUsed) = self.boolExp1.getText()
        (exp2Text, exp2LastValueUsed) = self.boolExp2.getText()
        text = exp1Text + exp2Text
        text += Output._or(exp1LastValueUsed, exp2LastValueUsed)
        AST_Node.recoverRecord(exp2LastValueUsed)
        return (text, exp1LastValueUsed)

class ExprNot(ExprBool):
    def __init__(self, boolExp):
        ExprBool.__init__(self)
        self.boolExp = boolExp

    def getText(self):
        (expText, lastValueUsed) = self.boolExp.getText()
        text = expText
        text += Output._not(lastValueUsed)
        return (text, lastValueUsed)

class ExprComparative(ExprBool):
    def __init__(self, exp1, exp2, condJump):
        ExprBool.__init__(self)
        self.exp1 = exp1
        self.exp2 = exp2
        self.condJump = condJump

    def getText(self):
        (exp1Text, exp1LastValueUsed) = self.exp1.getText()
        (exp2Text, exp2LastValueUsed) = self.exp2.getText()
        labelTrue = LABEL_IS_TRUE + str(AST_Node.tagIndex)
        labelFalse = LABEL_IS_FALSE + str(AST_Node.tagIndex)
        AST_Node.tagIndex += 1

        text = exp1Text + exp2Text
        text += Output.cmp(exp1LastValueUsed, exp2LastValueUsed)
        text += Output.condJump(self.condJump, labelTrue)
        text += Output.mov(exp1LastValueUsed, 0)
        text += Output.jmp(labelFalse)
        text += Output.tag_def(labelTrue)
        text += Output.mov(exp1LastValueUsed, -1)
        text += Output.tag_def(labelFalse)

        AST_Node.recoverRecord(exp2LastValueUsed)

        return (text, exp1LastValueUsed)

class ExprLe(ExprComparative):
    def __init__(self, numExp1, numExp2):
        ExprComparative.__init__(self, numExp1, numExp2, JLE)

class ExprGe(ExprComparative):
    def __init__(self, numExp1, numExp2):
        ExprComparative.__init__(self, numExp1, numExp2, JGE)

class ExprLt(ExprComparative):
    def __init__(self, numExp1, numExp2):
        ExprComparative.__init__(self, numExp1, numExp2, JL)

class ExprGt(ExprComparative):
    def __init__(self, numExp1, numExp2):
        ExprComparative.__init__(self, numExp1, numExp2, JG)

class ExprEq(ExprComparative):
    def __init__(self, exp1, exp2):
        ExprComparative.__init__(self, exp1, exp2, JE)

class ExprNe(ExprComparative):
    def __init__(self, exp1, exp2):
        ExprComparative.__init__(self, exp1, exp2, JNE)

class ExprAdd(ExprInt):
    def __init__(self, numExp1, numExp2):
        ExprInt.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def getText(self):
        (exp1Text, exp1LastValueUsed) = self.numExp1.getText()
        (exp2Text, exp2LastValueUsed) = self.numExp2.getText()
        text = exp1Text + exp2Text
        text += Output.add(exp1LastValueUsed, exp2LastValueUsed)
        AST_Node.recoverRecord(exp2LastValueUsed)
        return (text, exp1LastValueUsed)

class ExprSub(ExprInt):
    def __init__(self, numExp1, numExp2):
        ExprInt.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def getText(self):
        (exp1Text, exp1LastValueUsed) = self.numExp1.getText()
        (exp2Text, exp2LastValueUsed) = self.numExp2.getText()
        text = exp1Text + exp2Text
        text += Output.sub(exp1LastValueUsed, exp2LastValueUsed)
        AST_Node.recoverRecord(exp2LastValueUsed)
        return (text, exp1LastValueUsed)

class ExprMul(ExprInt):
    def __init__(self, numExp1, numExp2):
        ExprInt.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def getText(self):
        (exp1Text, exp1LastValueUsed) = self.numExp1.getText()
        (exp2Text, exp2LastValueUsed) = self.numExp2.getText()
        text = exp1Text + exp2Text
        text += Output.mov(RAX, exp1LastValueUsed)
        text += Output.mul(exp2LastValueUsed)
        AST_Node.recoverRecord(exp1LastValueUsed)
        AST_Node.recoverRecord(exp2LastValueUsed)
        return (text, RAX)

class Parameter(AST_Node):
    def __init__(self, name, pType):
        AST_Node.__init__(self)
        self.name = name
        self.type = pType

# types

class Type(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def __eq__(self, anotherType):
        return str(self) == str(anotherType)

    def __ne__(self, anotherType):
        return str(self) != str(anotherType)

    def __str__(self):
        return self.__class__.__name__

class Int(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Int'

class Bool(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Bool'

class Unit(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Unit'

class Vec(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Vec'