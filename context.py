from constants import *

class Context:
	def __init__(self):
		self.usedRecords = []
		self.reset()

	def reset(self):
		self.varIndex = 1
		self.paramIndex = 2
		self.params = {}
		self.vars = {}

	def getUsedRecords(self):
		return self.usedRecords

	def addVar(self, anId):
		if not anId in self.vars:
			self.vars[anId] = self.varIndex
			self.varIndex += 1
		return self.vars[anId]

	def addParam(self, anId):
		if not anId in self.params:
			self.params[anId] = self.paramIndex
			self.paramIndex += 1
		return self.params[anId]

	def getIdIndex(self, anId):
		if anId in self.params:
			return (self.params[anId], PARAM)
		else:
			return (self.vars[anId], VAR)

	def useRecord(self, record):
		self.usedRecords.append(record)

	def recoverRecord(self, record):
		if record in self.usedRecords:
			self.usedRecords.remove(record)