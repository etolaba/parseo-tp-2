from constants import *

class Output:
    indent = '    '

    @classmethod
    def text(cls, text):
        return cls.indent + text + '\n'

    @classmethod
    def unaryStmt(cls, op, exp):
        return cls.indent + op + ' ' + str(exp) + '\n'

    @classmethod
    def binaryStmt(cls, op, exp1, exp2):
        return cls.indent + op + ' ' + str(exp1) + ', ' + str(exp2) + '\n'

    @classmethod
    def cmp(cls, exp1, exp2):
        return Output.binaryStmt('cmp', exp1, exp2)

    @classmethod
    def mov(cls, dest, exp):
        return Output.binaryStmt('mov', dest, exp)
        
    @classmethod
    def movAssign(cls, index, exp):
        dest = '[' + RBP + ' - ' + str(index * BYTES) + ']'
        return Output.mov(dest, exp)

    @classmethod
    def movParam(cls, dest, index):
        exp = '[' + RBP + ' + ' + str(index * BYTES) + ']'
        return Output.mov(dest, exp)

    @classmethod
    def movVar(cls, dest, index):
        exp = '[' + RBP + ' - ' + str(index * BYTES) + ']'
        return Output.mov(dest, exp)

    @classmethod
    def fun_def(cls, funName, prefix='cuca_'):
        return prefix + funName + ':\n'

    @classmethod
    def tag_def(cls, tagName):
        return tagName + ':\n'

    @classmethod
    def call(cls, funName):
        return cls.indent + 'call ' + funName + '\n'

    @classmethod
    def cuca_call(cls, funName):
        return cls.indent + 'call cuca_' + funName + '\n'

    @classmethod
    def push(cls, recName):
        return Output.unaryStmt('push', recName)

    @classmethod
    def pop(cls, recName):
        return Output.unaryStmt('pop', recName)

    @classmethod
    def add(cls, exp1, exp2):
        return Output.binaryStmt('add', exp1, exp2)

    @classmethod
    def sub(cls, exp1, exp2):
        return Output.binaryStmt('sub', exp1, exp2)

    @classmethod
    def mul(cls, exp):
        return Output.unaryStmt('imul', exp)

    @classmethod
    def _and(cls, exp1, exp2):
        return Output.binaryStmt('and', exp1, exp2)

    @classmethod
    def _or(cls, exp1, exp2):
        return Output.binaryStmt('or', exp1, exp2)

    @classmethod
    def _not(cls, exp):
        return Output.unaryStmt('not', exp)

    @classmethod
    def jmp(cls, tag):
        return Output.unaryStmt('jmp', tag)

    @classmethod
    def condJump(cls, jump, tag):
        return Output.unaryStmt(jump, tag)

    @classmethod
    def inc(cls, exp):
        return Output.unaryStmt('inc', exp)

    @classmethod
    def sal(cls, exp1, exp2):
        return Output.binaryStmt('sal', exp1, exp2)

    @classmethod
    def vecStore(cls, exp1, exp2):
        return cls.indent + 'mov qword [' + str(exp1) + '], ' + str(exp2) + '\n'