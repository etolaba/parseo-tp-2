# Trabajo pr�ctico N� 2
### Compilaci�n de Cucaracha en Assembler para arquitectura x86-64

## Instalaci�n

### Para correr el TP necesitamos tener instalado:

- Python 2.7 (32 bits)
- PyParsing 2.1.8 (Compatible con Python 2.7)

## Uso

- Entramos al archivo `main.py` y editamos la variable `testNumber` para probar un test espec�fico
- Luego ejecutamos con Python el archivo `main.py`

## Aclaracion

- Al ejecutar el `main.py` se generar� un archivo `testXX.result` en la carpeta de los tests con el codigo Assembler