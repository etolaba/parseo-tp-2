RDI = 'rdi'
RSI = 'rsi'
RAX = 'rax'
RBX = 'rbx'
RCX = 'rcx'
RDX = 'rdx'
R8 = 'r8'
R9 = 'r9'
R10 = 'r10'
R11 = 'r11'
R12 = 'r12'
R13 = 'r13'
R14 = 'r14'
R15 = 'r15'
RBP = 'rbp'
RSP = 'rsp'

JMP = 'jmp'
JE = 'je'
JLE = 'jle'
JGE = 'jge'
JL = 'jl'
JG = 'jg'
JNE = 'jne'

#USABLERECORDS = [RDI, RSI, RAX, RBX, RCX, RDX, R8, R9, R10, R11, R12, R13, R14, R15]
USABLERECORDS = [R8, R9, R10, R11, R12, R13, R14, R15]

CUCA_PUTCHAR = 'putChar'
PUTCHAR = 'putchar'
CUCA_PUTNUM = 'putNum'
PRINTF = 'printf'
EXIT = 'exit'
LLI = 'lli_format_string'

PARAM = 'param'
VAR = 'var'

EXTERNFUNS = {
	CUCA_PUTCHAR: PUTCHAR, 
	CUCA_PUTNUM: PRINTF
}

BYTES = 8

LABEL_ELSE = '.label_else_'
LABEL_END = '.label_end_'
LABEL_START = '.label_start_'
LABEL_IS_TRUE = '.label_is_true_'
LABEL_IS_FALSE = '.label_is_false_'