from program_parser import Parser
from rules import PROGRAM as rules

if __name__ == '__main__':
	# Modificar el numero del test a correr
	testNumber = 0
	
	parser = Parser(rules)
	nString = str(testNumber)
	num = '0' + nString if testNumber < 10 else nString
	assembler = parser.parse('tests_compilador/test' + num + '.input')
	with open('tests_compilador/test' + num + '.result', 'w') as result:
		result.write(str(assembler))
